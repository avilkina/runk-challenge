# GithubRank challenge #

## Build & Run ##

```sh
$ cd runk-challenge
$ sbt
> jetty:start
> browse http://localhost:8080/users.html - will show all the urls with links that shows org info when clicking
> browse http://localhost:8080/users - returns response (login, name of orgs) in json format
> to test, run sbt from project folder then type ~ test to execute test
```

If `browse` doesn't launch your browser, manually open [http://localhost:8080/users.html](http://localhost:8080/users.html) or 
[http://localhost:8080/users](http://localhost:8080/users) in your browser.
