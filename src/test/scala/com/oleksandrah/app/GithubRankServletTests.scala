package com.oleksandrah.app

import org.scalatra.test.specs2._

class GithubRankServletTests extends ScalatraSpec { def is =
//Calls the pagesWork method to test whether pages work
  "GET / on GithubRankServlet" ^
    "should return status 200" ! pagesWork^
    end

  addServlet(classOf[GithubRankServlet], "/*")

//Follows the correct route
  def pagesWork = get("/users") {
    status must_== 200
  }


}