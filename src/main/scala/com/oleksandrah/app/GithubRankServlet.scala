package com.oleksandrah.app

import org.scalatra.ScalatraServlet
import org.scalatra.scalate.ScalateSupport
import org.scalatra.json._
import org.json4s.{DefaultFormats, Formats}
import com.oleksandrah.app.model.RankData.getOrg
import zio.ZIO
import zio.console.Console
import zio.DefaultRuntime
import com.oleksandrah.app.model.Org

// This is the main controller for app

class GithubRankServlet() extends ScalatraServlet with ScalateSupport with NativeJsonSupport {

  //Provides Json4s Formats
  protected implicit val jsonFormats: Formats = DefaultFormats

  val runtime = new DefaultRuntime{}


  before() {
    contentType = formats("json")
  }

  //Defines a route using the HTTP GET verb and a path (/)
  get("/users") {

  // Returns orgs in json format on http://localhost:8080/users
    val value: ZIO[Console, Throwable, Seq[Org]] = for {
      orgs <- getOrg
      _ <- ZIO.foreach(orgs)(org => zio.console.putStrLn(org.toString))
    } yield orgs

    runtime.unsafeRun(value.orDie)
  }

  get("/users.html") {
    contentType = "text/html"

   // http://localhost:8080/users.html - returns urls with links that shows orgs info when clicking
    val value: ZIO[Console, Throwable, List[scala.xml.Elem]] = for {
      orgs <- getOrg
      lis <- ZIO.foreach(orgs)(org => ZIO.succeed(<li><a href ={org.url}>{org.url}</a></li>))
    } yield lis

    <html>
      <head><title>Orgs</title></head>
      <body><ul> {runtime.unsafeRun(value.orDie)} </ul></body>
    </html>
  }

}
