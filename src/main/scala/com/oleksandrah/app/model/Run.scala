package com.oleksandrah.app.model

import com.oleksandrah.app.model.RankData.getOrg
import zio.console.Console
import zio.{Task, UIO, ZIO}

// we don't need that if we run our app from localhost

object Run extends zio.App {

  override def run(args: List[String]) = {
   val value: ZIO[Console, Throwable, Int] = for {
      orgs <- getOrg
      _ <- ZIO.collectAll(orgs.map(org => zio.console.putStrLn(org.toString)))
   } yield 0

    value.orDie
  }
}