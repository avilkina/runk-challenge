package com.oleksandrah.app.model
import sttp.client.{Request, ResponseError, basicRequest}
import org.json4s.native.Serialization
import sttp.client.asynchttpclient.zio.AsyncHttpClientZioBackend
import sttp.client._
import sttp.client.json4s.asJson
import zio.{UIO, ZIO}

//Organisation has login and url:
case class Org(login: String, url: String)
 {

 }

object RankData {

  //value for parameter serialization
  implicit val serialization: Serialization.type = org.json4s.native.Serialization

  type SimpleRequest[E, T] = Request[Either[ResponseError[E], T], Nothing]

  //Get request
  def getOrgsRequest(token: String): SimpleRequest[Exception, Seq[Org]] = basicRequest.get(uri"https://api.github.com/user/orgs")
    .header("Authorization", "token " + token)
    .response(asJson[Seq[Org]])

  //Get Token
  val getToken: UIO[String] = ZIO.effect(
    Option(System.getenv("GH_TOKEN"))).some
    .orElse(UIO("b64e81169cffc2f8258e1c836422d20ba8f3c864"))

  // Get Organisation
  val getOrg: ZIO[Any, Throwable, Seq[Org]] = (for {
    token <- getToken
    backend <- AsyncHttpClientZioBackend()
    r <- backend.send(getOrgsRequest(token))
  } yield {
    r.body
  }).absolve


}
