
import com.oleksandrah.app.GithubRankServlet

import org.scalatra.LifeCycle
import javax.servlet.ServletContext

class ScalatraBootstrap extends LifeCycle {
  override def init(context: ServletContext) {

//Creates the handler of the application and mounts it:
    context.mount(new GithubRankServlet, "/*")
  }
}
