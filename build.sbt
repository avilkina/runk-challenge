val ScalatraVersion = "2.7.0-RC1"

organization := "com.oleksandra"

name := "GithubRank challenge"

version := "1.0.0"

scalaVersion := "2.12.10"

resolvers += Classpaths.typesafeReleases

val sttp_version = "2.0.1"

libraryDependencies ++= Seq(
  "org.scalatra" %% "scalatra" % ScalatraVersion,
  "org.scalatra" %% "scalatra-specs2" % ScalatraVersion % "test",
  "ch.qos.logback" % "logback-classic" % "1.2.3" % "runtime",
  "org.eclipse.jetty" % "jetty-webapp" % "9.4.19.v20190610" % "container",
  "javax.servlet" % "javax.servlet-api" % "3.1.0" % "provided",
  "org.scalatra" %% "scalatra-scalate" % ScalatraVersion,
  "com.typesafe.slick" %% "slick" % "3.2.0",
  "com.h2database" % "h2" % "1.4.196",
  "com.mchange" % "c3p0" % "0.9.5.2",
  "org.scalatra" %% "scalatra-json" % ScalatraVersion,
  "org.json4s" %% "json4s-native" % "3.6.0",
  "dev.zio" %% "zio" % "1.0.0-RC17",
  "com.softwaremill.sttp.client" %% "async-http-client-backend-zio" % sttp_version,
  "com.softwaremill.sttp.client" %% "json4s" % sttp_version,
  "com.softwaremill.sttp.client" %% "core" % sttp_version,
  "org.scalatra" %% "scalatra-swagger"  % ScalatraVersion
)


resolvers +=
  "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"


enablePlugins(SbtTwirl)
enablePlugins(ScalatraPlugin)
